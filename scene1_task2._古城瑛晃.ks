
;■条件
;・使用キャラクターは自由
;・背景は何を使用してもOK

;■スクリプトを組む上でのポイント
;・抱き着く際の演出に違和感がないように
;・普通に組むと盛り上がりに欠けるので、どこかしらに演出を

*start

[cm]
[clearfix]
[chara_config pos_mode=false]

[position layer="message0" left=20 top=400 width=920 height=200 page=fore visible=true]

[position layer=message0 page=fore margint="45" marginl="50" marginr="70" marginb="60"]

[ptext name="chara_name_area" layer="message0" color="white" size=24 x=50 y=410]

[chara_config ptext="chara_name_area"]

;窶披披披披披披披蝿ﾈ下よりスクリプトを組んでください窶披披披披披披披髏

@layopt layer=message0 visible=true

;キャラ１定義
[chara_new name="akane" storage="chara/akane/normal.png" jname="キャラ１"]
;キャラ２定義
[chara_new name="hi" storage="chara/hi/hi01.png" jname="キャラ３"]
;キャラ１表情

[chara_face name="akane" face="happy" storage="chara/akane/happy.png" jname="キャラ１"]
[chara_face name="akane" face="normal" storage="chara/akane/normal.png" jname="キャラ１"]
[chara_face name="akane" face="angry" storage="chara/akane/angry.png" jname="キャラ１"]
[chara_face name="akane" face="back" storage="chara/akane/back.png" jname="キャラ１"]
;キャラ２表情
[chara_face name="hi" face="normal" storage="chara/hi/hi01.png" jname="キャラ１"]
[chara_face name="hi" face="surprise" storage="chara/hi/hi04.png" jname="キャラ１"]
[chara_face name="hi" face="akire" storage="chara/hi/hi05.png" jname="キャラ１"]
[chara_face name="hi" face="zito" storage="chara/hi/hi06.png" jname="キャラ１"]
[chara_face name="hi" face="angry" storage="chara/hi/hi07.png" jname="キャラ１"]

;背景


[playbgm storage="normal.ogg"]
[playse storage="suzume.ogg"]
[bg storage="gaikan_hiru.jpg" method="fadeIn"]
[chara_show name="akane" face="happy" left=280 top=50]
@layopt layer=message0 visible=true


;メニューボタン
@showmenubutton


[font size=35 color=white]

#キャラ１ 
キャラ３ちゃーん！[l]

[cm]

[chara_move name="akane" anim="true" time="500" effect="linear" wait="true" left="-500" ]
[chara_move name="akane" anim="false" time="0" wait="true" left="1000]
[chara_show name="hi" face="normal" left=280 top=50]

[chara_move name="akane" anim="true" time="500" effect="easeOutCirc" wait="true" left="440" ]

[chara_mod name="hi"  face="surprise" time="0"]

[anim name=hi left="-=30" top="+=5"  time=400 effect="easeOutCirc"]
[anim name=akane top="+=5"  time=400 effect="easeOutCirc"]
[wait time=100]
[anim name=hi left="+=5" time=100]
[anim name=aka left="+=5" time=100]
[wa]


#キャラ３ 
キャラ１！？[l]

[cm]

[chara_mod name="hi"  face="angry"]

#キャラ３ 
急に抱き着かないでよ！[l]

[cm]

[chara_mod name="akane"  face="angry"]

[anim name=akane left="-=20" time=200]
[anim name=hi left="-=10" time=200]
[wait time=100]
[anim name=akane left="+=20" time=200]
[anim name=hi left="+=10" time=200]
[wait time=100]
[anim name=akane left="-=20" time=200]
[anim name=hi left="-=10" time=200]
[wait time=100]
[anim name=akane left="+=20" time=200]
[anim name=hi left="+=10" time=200]
[wait time=200]

#キャラ１ 
えー！　いいじゃん[l]

[cm]

[mask time=700]
[chara_hide name="hi" time="0"]
[chara_hide name="akane" time="0"]


[chara_show name="akane" face="normal" left=460 top=50 time="0"]
[chara_show name="hi" face="normal" left=120 top=50 time="0"]

[mask_off time=1000]
[wait time=200]


#キャラ１ 
ところで、[r]
キャラ３ちゃんは何やってるの？[l]

[cm]

[chara_mod name="hi"  face="akire"]

#キャラ３ 
はぁ……、[r]
珍しい鳥がいたから見てたのよ[l]

[cm]

[chara_mod name="akane"  face="back"]


[anim name=akane left="-=20" time=200]
[wait time=100]
[anim name=akane left="+=20" time=200]
[wait time=100]
[anim name=akane left="-=20" time=200]
[wait time=100]
[anim name=akane left="+=20" time=200]
[wait time=200]


#キャラ１ 
え！　どこどこ！？[l]

[cm]

[chara_mod name="akane"  face="normal"]

#キャラ１ 
あ！　そういえば、これから新しく出来たカフェ行くんだけど、[r]一緒に行かない？[l]

[cm]

[chara_mod name="hi"  face="normal"]

#キャラ３ 
もう少し鳥を見たいから遠慮しとくわ[l]

[cm]

[chara_mod name="akane"  face="happy"]

[anim name=akane left="-=150"   time=400 effect="easeOutCirc"]
[wait time=100]
[anim name=hi left="-=5"  time=400 effect="easeOutCirc"]
#キャラ１ 
えー！　行こうよー[l]

[cm]

[chara_mod name="hi"  face="surprise"]

#キャラ３ 
ちょっと人前で恥ずかしいから辞めなさい！[l]

[cm]

[chara_mod name="hi"  face="angry"]

[font size=30 color=white]

[camera x=-80 y=70 zoom=1.6 time=300]


#キャラ３ 
と言うより……[r]
いつまで抱き着いてるのよ！[l]

[stopse]
[stopbgm]

[mask]
[chara_hide_all]
[chara_hide_all layer=1]
[freeimage layer="base"]
#
@layopt layer=message0 visible=false
[mask_off]
[jump storage=title.ks]
 

 